// Bài 2: Dùng arrow function & Rest param viết hàm tính DTB
function diemTB(...argument){
    let sum = 0;
    let dtb = 0;
    argument.forEach((item) => {
        sum += item;
        dtb = sum / argument.length;
    })
    return dtb;
}

function tinhDTBKhoi1(){
    let diemToan = document.getElementById('inpToan').value*1;
    let diemLy = document.getElementById('inpLy').value*1;
    let diemHoa = document.getElementById('inpHoa').value*1;
    
    let dtb = diemTB(diemToan,diemLy,diemHoa);
    document.getElementById('tbKhoi1').innerHTML = dtb.toFixed(2);
}

function tinhDTBKhoi2(){
    let diemVan = document.getElementById('inpVan').value*1;
    let diemSu = document.getElementById('inpSu').value*1;
    let diemDia = document.getElementById('inpDia').value*1;
    let diemTA = document.getElementById('inpEnglish').value*1;
    
    let dtb = diemTB(diemVan,diemSu,diemDia,diemTA);
    document.getElementById('tbKhoi2').innerHTML = dtb.toFixed(2);
}