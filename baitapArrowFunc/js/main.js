const colorList = ['pallet','viridian','pewter','cerulean','vermillion','lavender','celadon','saffron','fuschia','cinnabar'];
console.log('colorList :', colorList);

// Bài 1: arrow function load all color button
let addColorButton = (array) => {
    let contentHTML ='';
    for (var i = 0; i < array.length; i++){
        contentButton = `
        <button class="color-button ${array[i]}"></button>
        `;
        contentHTML += contentButton;
    }
    document.getElementById('colorContainer').innerHTML = contentHTML;
}
// load function
addColorButton(colorList);

// Bài 2: arrow function đổi màu ngôi nha theo màu click


// function set parent & sibling
const setActive = (element) => {
    [...element.parentElement.children].forEach((sibling) =>
    sibling.classList.remove("active")
    );
    element.classList.add("active");
};

// set active class & remove function + add corresponding color to object
let colorButtons = [...document.querySelectorAll(".color-button")];
let houseObject = document.getElementById('house');
console.log('colorButtons :', colorButtons);

colorButtons.forEach((button) => {
    // find the position of colro code in each button class (0): color-button (1): color code (2): active toggle
    let buttonColorPos = button.classList.item(1);

    console.log('buttonColorPos :', buttonColorPos);
    button.addEventListener("click", () => {
      setActive(button);
    // set color of current button to house
      houseObject.className = `house ${buttonColorPos}`
    });
});


