// Bài 3: Dùng hàm Spread tạo hiệu ứng từng chữ cái phóng to khi hover
let heading = document.querySelector('.heading').innerText;
console.log('heading :', heading);
let headingSpread = [...heading];
console.log('headingSpread :', headingSpread);
document.querySelector('.heading').innerText = '';
headingSpread.forEach((element) => {
    var contentText = 
    `
    <span>${element}</span>
    `;
    return document.querySelector('.heading').innerHTML += contentText;
});